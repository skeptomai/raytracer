#ifndef ITEMLISTH
#define ITEMLISTH

#include "item.h"

class item_list: public item  {
    public:
        item_list() {}
        item_list(item **l, int n) {list = l; list_size = n; }
        virtual bool hit(const ray& r, float tmin, float tmax, collision& rec) const;
        item **list;
        int list_size;
};

bool item_list::hit(const ray& r, float t_min, float t_max, collision& rec) const {
        collision temp_rec;
        bool hit_anything = false;
        double closest_so_far = t_max;
        for (int i = 0; i < list_size; i++) {
            if (list[i]->hit(r, t_min, closest_so_far, temp_rec)) {
                hit_anything = true;
                closest_so_far = temp_rec.t;
                rec = temp_rec;
            }
        }
        return hit_anything;
}

#endif