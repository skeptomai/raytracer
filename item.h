#ifndef ITEMH
#define ITEMH

#include "ray.h"

struct collision{
    float t;
    vec3 p;
    vec3 normal;
};

class item {
    public:
        virtual bool hit(const ray& r, float t_min, float t_max, collision& col) const = 0;
};


#endif